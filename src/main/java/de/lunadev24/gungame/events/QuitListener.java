package de.lunadev24.gungame.events;

import de.lunadev24.gungame.Main;
import de.lunadev24.gungame.util.GameState;
import de.lunadev24.gungame.player.PlayerManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Robin on 20.11.2015.
 */
public class QuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        if(Main.state != GameState.INGAME) {
            e.setQuitMessage(Main.pre + "§e" + p.getName() + " §7hat das Spiel verlassen");
            if(Main.online.contains(p)) {
                Main.online.remove(p);
            }
        }else{
            if(Main.online.contains(p)) {
                e.setQuitMessage(Main.pre + "§e" + p.getName() + " §7hat das Spiel verlassen");
                Main.online.remove(p);
            }
        }
        PlayerManager.removePlayerProfile(PlayerManager.getProfile(p));
    }

    @EventHandler
    public void onKick(PlayerKickEvent e) {
        Player p = e.getPlayer();
        if(Main.state != GameState.INGAME) {
            e.setLeaveMessage(Main.pre + "§e" + p.getName() + " §7hat das Spiel verlassen");
            if(Main.online.contains(p)) {
                Main.online.remove(p);
            }
        }else{
            if(Main.online.contains(p)) {
                e.setLeaveMessage(Main.pre + "§e" + p.getName() + " §7hat das Spiel verlassen");
                Main.online.remove(p);
            }
        }
        PlayerManager.removePlayerProfile(PlayerManager.getProfile(p));
    }

}
