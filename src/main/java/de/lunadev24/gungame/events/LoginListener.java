package de.lunadev24.gungame.events;

import de.lunadev24.gungame.Main;
import de.lunadev24.gungame.player.PlayerProfile;
import de.lunadev24.gungame.util.Countdown;
import de.lunadev24.gungame.util.GameState;
import de.lunadev24.gungame.player.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Robin on 20.11.2015.
 */
public class LoginListener implements Listener{

    @EventHandler
    public void onLogin(PlayerLoginEvent e) {
        Player p = e.getPlayer();
        if(Bukkit.getOnlinePlayers().size() >= 10) {
            //Usually: Check if Player is Premium
            Player kick = getRandomPlayer();
            kick.kickPlayer("Du wurdest von einem Premium gekickt");
            e.setResult(PlayerLoginEvent.Result.ALLOWED);
        }
        PlayerManager.addPlayerProfile(new PlayerProfile(p));
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        p.getInventory().clear();
        Arrays.stream(p.getInventory().getArmorContents()).forEach(n -> n.setType(Material.AIR));
        if(Main.state != GameState.INGAME) {
            e.setJoinMessage(Main.pre + "§e" + p.getName() + " §7hat das Spiel betreten");
            Main.online.add(p);
            if(Main.state == GameState.WAITING) {
                if(Bukkit.getOnlinePlayers().size() >= 2) {
                    Bukkit.getScheduler().cancelTasks(Main.getInstance());
                    Main.state = GameState.LOBBY;
                    Countdown.startLobby();
                }
            }
        }else{
            e.setJoinMessage(null);
            p.sendMessage(Main.pre + "�7Du bist nun Spectator");
        }
    }

    public Player getRandomPlayer() {
        Random r = new Random();
        int rnd = r.nextInt(Bukkit.getOnlinePlayers().size());
        return Main.online.get(rnd);
    }

}
