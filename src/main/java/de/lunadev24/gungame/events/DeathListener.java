package de.lunadev24.gungame.events;

import de.lunadev24.gungame.Main;
import de.lunadev24.gungame.mapsystem.MapManager;
import de.lunadev24.gungame.player.PlayerLevel;
import de.lunadev24.gungame.player.PlayerManager;
import de.lunadev24.gungame.player.PlayerProfile;
import de.lunadev24.gungame.util.Countdown;
import de.lunadev24.gungame.util.GameState;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

/**
 * Created by Robin on 20.11.2015.
 */
public class DeathListener implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();
        p.playSound(p.getLocation(), Sound.CHICKEN_HURT, 1F, 1F);
        e.getDrops().clear();
        if(Main.state == GameState.INGAME) {
            if(Main.online.contains(p)) {
                if(p.getKiller() != null) {
                    Player k = p.getKiller();
                    k.playSound(k.getLocation(), Sound.LEVEL_UP, 1F, 1F);
                    e.setDeathMessage(Main.pre + "§e" + p.getName() + " §7wurde von §e" + k.getName() + " §7getötet");
                    PlayerProfile pp = PlayerManager.getProfile(k);
                    if(pp.getLevel() == PlayerLevel.HIGH) {
                        Bukkit.broadcastMessage(Main.pre + "§e" + k.getName() + " §7hat das Spiel gewonnen");
                        Main.state = GameState.RESTART;
                        Countdown.startRestart();
                        for(Player all : Bukkit.getOnlinePlayers()) {
                            all.getInventory().clear();
                            all.updateInventory();
                        }
                    }else{
                        k.getInventory().clear();
                        pp.setLevel(PlayerLevel.getLevelFromID(pp.getLevel().getId() + 1));
                        k.sendMessage(Main.pre + "§7Dein neues Level: §e " + pp.getLevel().getId());
                        k.getInventory().setItem(0, pp.getLevel().getStack());
                        k.updateInventory();
                    }
                }else{
                    e.setDeathMessage(Main.pre + "§e" + p.getName() + " §7ist gestorben");
                }
                p.spigot().respawn();
            }else{
                p.setHealth(20.0D);
            }
        }else{
            p.setHealth(20.0D);
        }
    }

    @EventHandler
    public void onRespawn(PlayerRespawnEvent e) {
        Player p = e.getPlayer();
        e.setRespawnLocation(MapManager.getPlayedMap().getRandomLocation());
        PlayerProfile pp = PlayerManager.getProfile(p);
        if(pp.getLevel() != PlayerLevel.BASIC) {
            pp.setLevel(PlayerLevel.getLevelFromID(pp.getLevel().getId() - 1));
            p.sendMessage(Main.pre + "§7Dein neues Level: §e" + pp.getLevel());
        }
        p.getInventory().clear();
        p.getInventory().setItem(0, pp.getLevel().getStack());
    }

}
