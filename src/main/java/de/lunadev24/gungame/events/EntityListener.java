package de.lunadev24.gungame.events;

import de.lunadev24.gungame.Main;
import de.lunadev24.gungame.util.GameState;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

/**
 * Created by Robin on 20.11.2015.
 */
public class EntityListener implements Listener {

    @EventHandler
    public void onSpawn(CreatureSpawnEvent e) {
        if(e.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        if(e.getEntityType() == EntityType.PLAYER) {
            Player p = (Player) e.getEntity();
            if(Main.state == GameState.INGAME) {
                if(!Main.online.contains(p)) {
                    e.setCancelled(true);
                }
            }else {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onDamageByEntity(EntityDamageByEntityEvent e) {
        if(e.getEntityType() == EntityType.PLAYER) {
            if(e.getDamager().getType() == EntityType.PLAYER) {
                if(Main.state == GameState.INGAME) {
                    Player p = (Player) e.getEntity();
                    Player d = (Player) e.getDamager();
                    if (!(Main.online.contains(p) && Main.online.contains(d))) {
                        e.setCancelled(true);
                    }
                }else{
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onChange(FoodLevelChangeEvent e) {
        e.setCancelled(true);
    }

}
