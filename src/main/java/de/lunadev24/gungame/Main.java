package de.lunadev24.gungame;

import de.lunadev24.gungame.commands.SetupMapCommand;
import de.lunadev24.gungame.commands.WorldCommand;
import de.lunadev24.gungame.events.*;
import de.lunadev24.gungame.mapsystem.MapManager;
import de.lunadev24.gungame.util.AsyncMySQL;
import de.lunadev24.gungame.util.Countdown;
import de.lunadev24.gungame.util.GameState;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Robin on 20.11.2015.
 */

public class Main extends JavaPlugin {

    public static String pre = "§7[§5GunGame§7] ";
    private static Main instance;
    private static YamlConfiguration cfg;
    private static AsyncMySQL mysql;

    public static List<Player> online;
    public static GameState state;

    public void onEnable() {
        instance = this;
        registerEvents();
        online = new ArrayList<>();
        state = GameState.WAITING;
        registerCommands();
        registerEvents();
        loadConfiguration();
        MapManager.setup();
        new Countdown(61, 21);
        Countdown.startWaiting();
        System.out.println("[GunGame] Plugin activated");
    }

    public void loadConfiguration() {
        File file = new File("plugins/GunGame", "config.yml");
        cfg = YamlConfiguration.loadConfiguration(file);
        if(file.exists()) {
            String host = cfg.getString("MySQL.Host");
            int port = cfg.getInt("MySQL.Port");
            String user = cfg.getString("MySQL.User");
            String password = cfg.getString("MySQL.Password");
            String database = cfg.getString("MySQL.Datenbank");
            try {
                mysql = new AsyncMySQL(host, port, user, password, database);
            } catch (Exception e) {
                System.out.println("Bitte gebe zuerst richtige MySQL Daten an!");
                Bukkit.shutdown();
            }
            mysql.update("CREATE TABLE IF NOT EXISTS gungame(useable INT, name TEXT, builder TEXT, locations TEXT)");
        }else{
            cfg.set("MySQL.Host", "localhost");
            cfg.set("MySQL.Port", 3306);
            cfg.set("MySQL.User", "Benutzername");
            cfg.set("MySQL.Password", "Passwort");
            cfg.set("MySQL.Datenbank", "Datenbank");
            try {
                cfg.save(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void onDisable() {
        System.out.println("[GunGame] Plugin deactivated");
    }

    public void registerCommands() {
        getCommand("setupmap").setExecutor(new SetupMapCommand());
        getCommand("world").setExecutor(new WorldCommand());
    }

    public void registerEvents() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new BlockListener(), this);
        pm.registerEvents(new DeathListener(), this);
        pm.registerEvents(new EntityListener(), this);
        pm.registerEvents(new LoginListener(), this);
        pm.registerEvents(new QuitListener(), this);
    }

    public static Main getInstance() {
        return instance;
    }

    public static AsyncMySQL getMySQL() {
        return mysql;
    }

}
