package de.lunadev24.gungame.commands;

import de.lunadev24.gungame.Main;
import de.lunadev24.gungame.mapsystem.MapManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class SetupMapCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;
            if(p.isOp()) {
                if(args.length == 2) {
                    String name = args[0];
                    if(args[1].equalsIgnoreCase("create")) {
                        if(!MapManager.existsArena(name)) {
                            MapManager.initializeNewArena(name);
                            p.sendMessage(Main.pre + "§7Die Arena §e" + name + " §7ist nun aufgesetzt und kann eingerichtet werden");
                        }else{
                            p.sendMessage(Main.pre + "§7Diese Arena existiert schon");
                        }
                    }else if(args[1].equalsIgnoreCase("setUseable")) {
                        if(MapManager.existsArena(name)) {
                            if(MapManager.isComplete(name)) {
                                Main.getMySQL().update("UPDATE gungame SET useable='" + 1 + "' WHERE name='" + name + "'");
                                p.sendMessage(Main.pre +  "§7Die Map §e" + name + " §7ist nun spielbar");
                            }else{
                                p.sendMessage(Main.pre + "§7Die Map ist nicht vollständig eingerichtet");
                            }
                        }else{
                            p.sendMessage(Main.pre + "§7Diese Arena existiert nicht");
                        }
                    }
                }else if(args.length == 3) {
                    String name = args[0];
                    if(args[1].equalsIgnoreCase("setBuilder")) {
                        if(MapManager.existsArena(name)) {
                            String builder = args[2];
                            MapManager.setBuilder(name, builder);
                            p.sendMessage(Main.pre + "§7Der Builder der Map §e" + name + " §7ist nun §e" + builder);
                        }else{
                            p.sendMessage(Main.pre + "§7Diese Arena existiert nicht");
                        }
                    }else if(args[1].equalsIgnoreCase("setSpawn")) {
                        if(MapManager.existsArena(name)) {
                            int id = 0;
                            try {
                                id = Integer.valueOf(args[2]);
                            } catch (NumberFormatException e) {
                                p.sendMessage(Main.pre + "§7Gebe eine Zahl an");
                                return true;
                            }
                            if (id > 0 && id <= 20) {
                                MapManager.setSpawn(name, p.getLocation(), id);
                                p.sendMessage(Main.pre + "§7Der Spawn §e" + id + " §7auf der Map §e" + name + " §7wurde gesetzt");
                            } else {
                                p.sendMessage(Main.pre + "§7Wie hat denn der olle Sahchse ein einreiseschein in westen bekommen ?schen §e1 §7und §e20 §7an");
                            }
                        }else{
                            p.sendMessage(Main.pre + "§7Diese Arena existiert nicht");
                        }
                    }
                }
            }
        }else{
            sender.sendMessage("Nur für Spieler");
        }
        return true;
    }

    private void sendHelpMessage(Player p) {
        p.sendMessage("§e-><-§7SetupMap Hilfe §e-><-");
        p.sendMessage("§e/setupmap <Name> create §7Erstellt eine neue Arena");
        p.sendMessage("§e/setumap <Name> setBuilder <Name> §7Setzt den Namen des Builders");
        p.sendMessage("§e/setupmap <Name> setSpawn <ID> §7Setzt den Spawn");
        p.sendMessage("§e/setupmap <Name> setUseable §7Macht die Map spielbar");
    }

}
