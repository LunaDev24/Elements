package de.lunadev24.gungame.commands;

import de.lunadev24.gungame.Main;
import de.lunadev24.gungame.mapsystem.MapManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WorldCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player) sender;
			if(p.isOp()) {
				if(args.length == 2) {
					if(args[0].equalsIgnoreCase("tp")) {
						if(Bukkit.getWorld(args[1]) != null) {
							p.teleport(Bukkit.getWorld(args[1]).getSpawnLocation());
						}else{
							p.sendMessage(Main.pre + "§7Die Welt existiert nicht");
						}
					}else if(args[0].equalsIgnoreCase("load")) {
						if(Bukkit.getWorld(args[1]) != null) {
							p.sendMessage(Main.pre + "§7Die Welt existiert bereitss");
						}else{
							MapManager.loadWorld(args[1]);
							p.sendMessage(Main.pre + "§7Die Welt wurde geladen");
						}
					}else{
						sendHelpMessage(p);
					}
				}
			}
		}else{
			sender.sendMessage("Nur für Spieler");
		}
		return true;
	}

	private void sendHelpMessage(Player p) {
		p.sendMessage(Main.pre + "§e------Liste aller /world-Commands------");
		p.sendMessage(Main.pre + "§e/world create <Name> Erstellt bzw lädt die genannte Welt");
		p.sendMessage(Main.pre + "§e/world tp <Name> Teleportiert den Spieler in die genannte Welt");
		p.sendMessage(Main.pre + "§e/world list Liste aller geladenen Welten");
		p.sendMessage(Main.pre + "§e/world setSpawnLocation Setzt die Spawn-Location in deiner Welt");
	}
	
}
