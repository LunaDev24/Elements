package de.lunadev24.gungame.mapsystem;

import de.lunadev24.gungame.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class LocationManager {

	/*
		Anmerkung:
		Das ist eine ziemlich alte Klasse, welche ich aus Faulheit heute immer noch nutze
		Eine neue habe ich bereits, welche ich allerdings zusammen mit einem
		anderen Developer geschrieben habe, und ich deswegen hier nicht
		mit einbinden möchte
	 */
	
	private static ArrayList<PluginLocation> locs = new ArrayList<PluginLocation>();
	
	public static void addLocation(String name,Location loc) {
		PluginLocation ploc = new PluginLocation(name, loc);
		locs.add(ploc);
	}
	
	public static Location getLocation(String name) {
		for(PluginLocation pls : locs) {
			if(pls.getName().equalsIgnoreCase(name)) {
				return pls.getLocation();
			}
		}
		return null;
	}
	
	public static boolean isDefinedLocation(String name) {
		try{
			ResultSet rs = Main.getMySQL().getMySQL().query("SELECT location FROM locations WHERE name = '" + name + "'");
			while(rs.next()) {
				return true;
			}
		}catch(SQLException ex) {
			
		}
		return false;
	}
	
	public static Location getUndefinedLocation(String name) {
		String rawlocation = null;
		try {
			ResultSet rs = Main.getMySQL().getMySQL().query("SELECT location FROM locations WHERE name = '" + name + "'");
			while(rs.next()) {
				rawlocation = rs.getString(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return StringToLocation(rawlocation);
	}
	
	public static void setLocation(String name, Location loc, boolean yaw_pitch) {
		if(!isDefinedLocation(name)) {
			Main.getMySQL().update("INSERT INTO locations (name,location) values ('" + name + "' , '" + LocationToString(yaw_pitch, loc) + "')");
		}else{
			Main.getMySQL().update("UPDATE locations SET location= '" + LocationToString(yaw_pitch, loc) + "' WHERE name= '" + name + "'");
		}
	}	
	
	public static Location StringToLocation(String rawlocation) {
		if(rawlocation != null) {
			String[] locates = rawlocation.split(",");
			String yaw_pitch = locates[0];
			String world = locates[1];
			String rx = locates[2];
			String ry = locates[3];
			String rz = locates[4];
			double x = Double.parseDouble(rx);
			double y = Double.parseDouble(ry);
			double z = Double.parseDouble(rz);
			Location loc = new Location(Bukkit.getWorld(world), x, y, z);
			if(yaw_pitch.equalsIgnoreCase("true")) {
				String ryaw = locates[5];
				String rpitch = locates[6];
				float yaw = Float.parseFloat(ryaw);
				float pitch = Float.parseFloat(rpitch);
				loc.setPitch(pitch);
				loc.setYaw(yaw);
			}
			return loc;
		}else{
			return null;
		}
	}
	
	public static String LocationToString(boolean yaw_pitch, Location rawlocation) {
		String toreturn = "true," + rawlocation.getWorld().getName() + ","
				+ String.valueOf(rawlocation.getX()) + ","
				+ String.valueOf(rawlocation.getY()) + ","
				+ String.valueOf(rawlocation.getZ());
		if(yaw_pitch) {
			toreturn = toreturn + ","
			+ String.valueOf(rawlocation.getYaw()) + ","
			+ String.valueOf(rawlocation.getPitch());
		}
		return toreturn;
	}
	
	public static World loadWorld(String WorldName) {
		if (!(WorldName == null)) {
			World world = Bukkit.getWorld(WorldName);
			WorldCreator creator = new WorldCreator(WorldName);
			creator.environment(World.Environment.NORMAL);
			creator.generateStructures(true);
			world = creator.createWorld();
			return world;
			
		}
		return null;
	}

}
