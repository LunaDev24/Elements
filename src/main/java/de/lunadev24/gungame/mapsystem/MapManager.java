package de.lunadev24.gungame.mapsystem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import de.lunadev24.gungame.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;

public class MapManager {
	
	public static ArrayList<PluginMap> maps = new ArrayList<>();
	private static PluginMap playedMap;
	private static String dm;
	
	public static PluginMap getPlayedMap() {
		return playedMap;
	}
	
	public static void setPlayedArena(final String n) {
		ResultSet rs = Main.getMySQL().getMySQL().query("SELECT * FROM gungame WHERE name='" + n + "'");
		try{
			while(rs.next()) {
				String builder = rs.getString("builder");
				String locations = rs.getString("locations");
				String[] spawns = locations.split(";");
				String[] locates = spawns[0].split(",");
				loadWorld(locates[1]);
				playedMap = new PluginMap(n, builder, locations);
			}
		}catch(SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	public static void setup() {
		String map = null;
		ResultSet rs = Main.getMySQL().getMySQL().query("SELECT name FROM gungame WHERE useable='" + 1 + "' ORDER BY RAND() LIMIT 1");
		try{
			while(rs.next()) {
				map = rs.getString("name");
			}
		}catch(SQLException ex) {
			ex.printStackTrace();
		}
		setPlayedArena(map);
	}
	
	public static boolean existsArena(String name) {
		try{
			ResultSet rs = Main.getMySQL().getMySQL().query("SELECT useable FROM gungame WHERE name= '" + name + "'");
			while(rs.next()) {
				return true;
			}
		}catch(SQLException ex) {
			ex.printStackTrace();
		}
		return false;
	}
	
	public static String generateRawInput(int size) {
		StringBuilder str = new StringBuilder();
		for(int i = 0; i < size; i++) {
			if(i == size - 1) {
				str.append("null");
			}else{
				str.append("null;");
			}
		}
		return str.toString();
	}
	
	public static void initializeNewArena(String name) {
		Main.getMySQL().update("INSERT INTO gungame (useable,name,builder,locations) values ('" + 0 + "' , '" + name + "' , '" + "Builder" + "' , '" + generateRawInput(10) + "')");
	}
	
	public static void setBuilder(final String name, final String builder) {
		Main.getMySQL().query("SELECT builder FROM gungame WHERE name='" + name + "'", rs -> {
			try{
				while(rs.next()) {
					Main.getMySQL().update("UPDATE gungame SET builder='" + builder + "'WHERE name='" + name + "'");
				}
			}catch(SQLException ex) {
				ex.printStackTrace();
			}
		});
	}
	
	public static boolean isComplete(String name) {
		try{
			ResultSet rs = Main.getMySQL().getMySQL().query("SELECT * FROM gungame WHERE name='" + name + "'");
			while(rs.next()) {
				int useable = rs.getInt("useable");
				if(useable == 0) {				
					String builder = rs.getString("builder");
					String locs = rs.getString("locations");
					if((!builder.equalsIgnoreCase("Builder")) && (!locs.contains("null"))) {
						return true;
					}else{
						return false;
					}
				}
			}
			return false;
		}catch(SQLException ex) {
			ex.printStackTrace();
		}
		return false;
	}
	
	public static void setSpawn(final String name, final Location loc, final int number) {
		Main.getMySQL().query("SELECT locations FROM gungame WHERE name='" + name + "'", rs -> {
			try{
				while(rs.next()) {
					String locs = rs.getString("locations");
					String[] locsingle = locs.split(";");
					int n = number - 1;
					locsingle[n] = LocationManager.LocationToString(true, loc);
					String toreturn = null;
					for (int i = 0; i < locsingle.length; i++) {
						if(toreturn == null) {
							toreturn = locsingle[i] + ";";
						}else{
							int last = locsingle.length - 1;
							if(i == last) {
								toreturn = toreturn + locsingle[i];
							}else{
								toreturn = toreturn + locsingle[i] + ";";
							}
						}
					}
					Main.getMySQL().update("UPDATE gungame SET locations='" + toreturn + "' WHERE name='" + name + "'");
				}
			}catch(SQLException ex) {
				ex.printStackTrace();
			}
		});
	}
	
	public static World loadWorld(String WorldName) {
		if (WorldName != null) {
			World world = Bukkit.getWorld(WorldName);
			WorldCreator creator = new WorldCreator(WorldName);
			creator.environment(World.Environment.NORMAL);
			creator.generateStructures(true);
			world = creator.createWorld();

			return world;
		}
		return null;
	}

}
