package de.lunadev24.gungame.mapsystem;

import lombok.Getter;
import org.bukkit.Location;

@Getter
public class PluginLocation {
	
	private Location location;
	private String name;
	
	public PluginLocation(String name, Location loc) {
		this.name = name;
		this.location = loc;
	}

}
