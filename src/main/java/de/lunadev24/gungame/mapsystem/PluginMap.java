package de.lunadev24.gungame.mapsystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import lombok.Getter;
import org.bukkit.Location;

@Getter
public class PluginMap {
	
	private String name;
	private String builder;
	private List<Location> locations;
	
	public PluginMap(String name, String builder, String spawns) {
		this.name = name;
		this.builder = builder;
		String[] rawspawns = spawns.split(";");
		ArrayList<Location> tor = new ArrayList<>();
		for(String s : rawspawns) {
			tor.add(LocationManager.StringToLocation(s));
		}
		this.locations = tor;
	}

	public Location getRandomLocation() {
		Random r = new Random();
		int rnd = r.nextInt(locations.size());
		return locations.get(rnd);
	}

}
