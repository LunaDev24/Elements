package de.lunadev24.gungame.player;

import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Robin on 20.11.2015.
 */
@Getter
public enum PlayerLevel {

    BASIC(1, Material.WOOD_SWORD), MIDDLE(2, Material.STONE_SWORD), HIGH(3, Material.IRON_SWORD);

    private int id;
    private Material material;

    private PlayerLevel(int id, Material material) {
        this.id = id;
        this.material = material;
    }

    public ItemStack getStack() {
        return new ItemStack(material);
    }

    public static PlayerLevel getLevelFromID(int id) {
        for(PlayerLevel level : values()) {
            if(level.getId() == id) {
                return level;
            }
        }
        return BASIC;
    }

}
