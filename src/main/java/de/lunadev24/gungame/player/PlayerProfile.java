package de.lunadev24.gungame.player;

import lombok.Getter;
import org.bukkit.entity.Player;

/**
 * Created by Robin on 20.11.2015.
 */

@Getter
public class PlayerProfile {

    private Player player;
    private PlayerLevel level;

    public PlayerProfile(Player player) {
        this.player = player;
        this.level = PlayerLevel.BASIC;
    }

    public void setLevel(PlayerLevel level) {
        this.level = level;
    }

}
