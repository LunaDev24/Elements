package de.lunadev24.gungame.player;

import org.bukkit.entity.Player;

import java.util.HashMap;

/**
 * Created by Robin on 20.11.2015.
 */
public class PlayerManager {

    private static HashMap<Player, PlayerProfile> players = new HashMap<>();

    public static void addPlayerProfile(PlayerProfile p) {
        players.put(p.getPlayer(), p);
    }

    public static void removePlayerProfile(PlayerProfile p) {
        players.remove(p);
    }

    public static PlayerProfile getProfile(Player p) {
        if(players.containsKey(p)) {
            return players.get(p);
        }
        return null;
    }

}
