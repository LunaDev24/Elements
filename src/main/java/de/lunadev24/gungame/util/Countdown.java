package de.lunadev24.gungame.util;

import de.lunadev24.gungame.Main;
import de.lunadev24.gungame.mapsystem.MapManager;
import de.lunadev24.gungame.player.PlayerLevel;
import de.lunadev24.gungame.player.PlayerProfile;
import de.lunadev24.gungame.player.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

/**
 * Created by Robin on 20.11.2015.
 */
public class Countdown  {

    private static int LOBBY;
    private static int RESTART;

    /**
     * Start Countdown for GameState.WARTEN GameState
     * @param lobby Lobby Timer
     * @param restart Restart Timer
     */
    public Countdown(int lobby, int restart) {
        LOBBY = lobby;
        RESTART = restart;
    }


    /**
     * Start Countdown for GameState.WARTEN GameState
     */
    public static void startWaiting() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), () -> {

            if(Main.state == GameState.WAITING) {
                Bukkit.broadcastMessage(Main.pre + "§7Warte auf weitere Spieler");
            }

        }, 0L, 10 * 20L);
    }
    /**
     * Start Countdown for GameState.LOBBY GameState
     */
    public static void startLobby() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), () -> {

            if(Main.state == GameState.LOBBY) {
                LOBBY--;
                if(LOBBY == 0) {
                    Bukkit.getScheduler().cancelTasks(Main.getInstance());
                    if(MapManager.getPlayedMap() != null) {
                        for (int i = 0; i < Main.online.size(); i++) {
                            Player p = Main.online.get(i);
                            PlayerProfile pp = PlayerManager.getProfile(p);
                            pp.setLevel(PlayerLevel.BASIC);
                            p.getInventory().setItem(0, pp.getLevel().getStack());
                            p.teleport(MapManager.getPlayedMap().getLocations().get(i));
                        }
                        Main.state = GameState.INGAME;
                    }else{
                        Bukkit.broadcastMessage(Main.pre + "§7Es wurde keine Map geladen!");
                        Bukkit.shutdown();
                    }
                }
                if(LOBBY == 60 || LOBBY == 30 || LOBBY == 15 || LOBBY == 10 || (LOBBY <= 5 && LOBBY > 0)) {
                    for(Player a : Bukkit.getOnlinePlayers()) {
                        a.playSound(a.getLocation(), Sound.ORB_PICKUP, 1F, 1F);
                    }
                    if(LOBBY == 1) {
                        Bukkit.broadcastMessage(Main.pre + "§7Das Spiel startet in §e" + LOBBY + " §7Sekunde");
                        return;
                    }
                    Bukkit.broadcastMessage(Main.pre + "§7Das Spiel startet in §e" + LOBBY + " §7Sekunden");
                }
            }

        }, 0L, 20L);
    }
    /**
     * Start Countdown for GameState.RESTART GameState
     */
    public static void startRestart() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), () -> {

            if(Main.state == GameState.RESTART) {
                RESTART--;
                if(RESTART == 0) {
                    Bukkit.spigot().restart();
                    return;
                }
                if(RESTART == 20 || RESTART == 10 || (RESTART <= 5 && RESTART > 0)) {
                    if(RESTART == 1) {
                        Bukkit.broadcastMessage(Main.pre + "�7Der Server startet in �c" + RESTART + " �7Sekunde neu");
                        return;
                    }
                    Bukkit.broadcastMessage(Main.pre + "�7Der Server startet in �c" + RESTART + " �7Sekunden neu");
                }
            }

        }, 0L, 20L);
    }

}
