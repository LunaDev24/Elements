package de.lunadev24.gungame.util;

/**
 * Created by Robin on 20.11.2015.
 */
public enum GameState {

    WAITING, LOBBY, INGAME, RESTART;

}
